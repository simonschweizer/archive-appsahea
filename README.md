# Develop README

Dieser Branch enthält nur GitLab-spezifische Dateien (Templates für Issues und MRs).


Der Quellcode der einzelnen Teilprojekte befindet sich auf den jeweiligen Feature-Branches.

| Branchname        | Inhalt                                                                      |
|-------------------|-----------------------------------------------------------------------------|
| `app/develop`     | Quellcode, Layout der App, ohne Services                                    |
| `common/develop`  | Services, Modellklassen, Rest-Client                                        |
| `backend/develop` | REST-Services, Service-Implementierung (JDBC), Zertifizierungsstelle (noch) |
| `website/develop` | Website                                                                     |
