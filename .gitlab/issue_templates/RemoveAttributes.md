Datenfelder sollten nur als solche vorhanden sein, wenn die Daten in anderen Methoden / Klassen benötigt werden und es Sinn macht, die Daten nicht nur in einer bestimmten Methode vorzuhalten (als lokale Variable).

Falls dieser Zweck nicht gegeben ist, dann bitte entfernen oder als lokale Variable vorhalten (es kommt im Projekt auch vor, dass Datenfelder nur zugewiesen werden, aber niemals abgefragt).

Falls es deiner Meinung nach Sinn macht, dann bitte dieses Issue schließen und den Begründung zusammen mit Klassen- und Datenfeldnamen kommentieren (aber nicht einfach so schließen).

Es handelt sich um folgende Datenfelder:
- `Klasse`: `Datenfeld`
- `Klasse`: `Datenfeld`

Zu beachten sind wenn möglich auch immer die [Coderegeln](https://git.it.hs-heilbronn.de/swp23-ss17/team2-diabetes/wikis/aufraeumen/Coderegeln).
Die Commitkommentare sollten sich immer auf das aktuelle Issue beziehen: `fixes #IssueNummer`.
Durch `fixes` wird automatisch dieses Issue geschlossen, deshalb bitte an diese Konvention halten.
