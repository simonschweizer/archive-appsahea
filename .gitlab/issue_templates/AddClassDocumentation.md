Es fehlt noch das JavaDoc zur oben genannten Klasse im Modul `appsahea-app`. Dieses sollte zumindest den Autor der Klasse und die Beschreibung der Klasse beinhalten, damit klar ist, was genau durch diese Klasse gelöst werden soll und wie sie sich eventuell von ähnlichen Klassen im Projekt oder in Bibliotheken (die wir einsetzen) unterscheidet.

Da die gesamte Implementierung in Englisch gehalten ist, muss natürlich auch die Dokumentation auf Englisch verfasst sein.

Hier geht es nicht um die Dokumentation von den Methoden oder Datenfeldern - diese sollten auch dokumentiert werden, falls sie dafür gedacht sind, von anderen Klassen aufgerufen zu werden.

Bitte denke daran, dass auch interne Klassen dokumentiert werden müssen.

Zu beachten sind wenn möglich auch immer die [Coderegeln](https://git.it.hs-heilbronn.de/swp23-ss17/team2-diabetes/wikis/aufraeumen/Coderegeln).
Die Commitkommentare sollten sich immer auf das aktuelle Issue beziehen: `fixes #IssueNummer`.
Durch `fixes` wird automatisch dieses Issue geschlossen, deshalb bitte an diese Konvention halten.
