Der Aufruf einer Methode liefert nicht das gewünschte Ergebnis.

Name des Services: `UserService`

Name der Methode: `Methodenname`

Schritte zum Reproduzieren:
1. einen User anlegen
2. diesen Nutzer löschen

Verwendete Parameter: die User-ID des gerade angelegten Patienten

Erwartetes Ergebnis: Benutzer befindet sich nicht mehr in der Benutzerliste

Reales Ergebnis: Benutzer befindet sich immer noch in der Benutzerliste

Der Test befindet sich auf dem Branch `branchname`. Ich habe zuvor die aktuellen Änderungen aus dem Branch `backend/develop` und `backend/tests` mit meinem lokalen Branch gemergt.

Der Test befindet sich in der Klasse: `de.hhn.swp23.ss17.diabetes.servicetests.XXXServiceTest`

Name der Testmethode: `testGetDeletedUser`

---

- [X] Ich denke, dass es sich um einen Fehler der Service-Implementierung handelt
- [ ] Ich habe Bedenken, ob ich den Test richtig geschrieben habe
- [ ] Ich weiß, woran der Fehler liegt: 
- [ ] Ich habe den Fehler behoben, der Patch befindet sich im Branch `backend/...`

---

Sonstige Notizen: keine